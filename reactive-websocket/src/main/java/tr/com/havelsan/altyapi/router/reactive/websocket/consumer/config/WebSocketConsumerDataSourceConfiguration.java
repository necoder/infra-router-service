package tr.com.havelsan.altyapi.router.reactive.websocket.consumer.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import tr.com.havelsan.altyapi.router.api.config.DataSourceConfiguration;
import tr.com.havelsan.altyapi.router.reactive.websocket.model.WebSocketDataSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@Configuration
@ConfigurationProperties(prefix = "router.consumer.reactive-websocket")
public class WebSocketConsumerDataSourceConfiguration implements DataSourceConfiguration<WebSocketDataSource> {
    private List<WebSocketDataSource> dataSources;

    @Override
    public List<WebSocketDataSource> getDataSources() {
        return Optional.ofNullable(dataSources)
                .orElse(new ArrayList<>());
    }
}
