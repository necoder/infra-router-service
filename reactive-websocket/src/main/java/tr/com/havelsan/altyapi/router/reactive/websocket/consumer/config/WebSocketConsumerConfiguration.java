package tr.com.havelsan.altyapi.router.reactive.websocket.consumer.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import tr.com.havelsan.altyapi.router.api.IRouterManager;
import tr.com.havelsan.altyapi.router.api.config.DataSource;
import tr.com.havelsan.altyapi.router.api.config.ServiceConfiguration;
import tr.com.havelsan.altyapi.router.reactive.websocket.consumer.service.WebSocketConsumerService;
import tr.com.havelsan.altyapi.router.reactive.websocket.model.WebSocketDataSource;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@Configuration
public class WebSocketConsumerConfiguration implements ServiceConfiguration {
    private final WebSocketConsumerDataSourceConfiguration dataSourceConfig;
    private final Map<String, WebSocketHandler> urlMap;

    public WebSocketConsumerConfiguration(WebSocketConsumerDataSourceConfiguration dataSourceConfig) {
        this.dataSourceConfig = dataSourceConfig;
        this.urlMap = new HashMap<>();
    }

    @Override
    @Autowired
    public void registerServicesToRouter(IRouterManager routerManager) {
        dataSourceConfig.getDataSources()
                .stream()
                .peek(dataSource -> log.info("Data Source name:{}, enabled:{}.",
                        dataSource.getName(),
                        dataSource.getEnabled()))
                .filter(DataSource::getEnabled)
                .forEach(dataSource -> {
                    WebSocketConsumerService webSocketConsumerService = new WebSocketConsumerService(dataSource.getName());
                    log.info("Websocket Consumer Service({}) is registering to router..", webSocketConsumerService.getName());
                    routerManager.registerConsumer(webSocketConsumerService);
                    urlMap.put(dataSource.getUrl(), webSocketConsumerService);
                });
    }

    @Bean
    public HandlerMapping webSocketHandlerMapping() {
        SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping(urlMap, 1);
        handlerMapping.setCorsConfigurations(getCorsConfigurationMap());
        return handlerMapping;
    }

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }

    private Map<String, CorsConfiguration> getCorsConfigurationMap() {
        return dataSourceConfig.getDataSources()
                .stream()
                .filter(DataSource::getEnabled)
                .collect(Collectors.toMap(WebSocketDataSource::getUrl, WebSocketDataSource::getCorsConfig));
    }
}