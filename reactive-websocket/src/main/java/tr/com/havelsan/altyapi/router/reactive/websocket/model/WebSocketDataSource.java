package tr.com.havelsan.altyapi.router.reactive.websocket.model;

import lombok.Data;
import org.springframework.web.cors.CorsConfiguration;
import tr.com.havelsan.altyapi.router.api.config.DataSource;

import java.util.Optional;

@Data
public class WebSocketDataSource implements DataSource {
    private final static String DEFAULT_NAME = "reactive-websocket-service";
    private final static Boolean DEFAULT_ENABLED = false;
    private final static String DEFAULT_URL = "default-url";
    private final static CorsConfiguration DEFAULT_CORS_CONFIG = new CorsConfiguration();

    private String name;
    private Boolean enabled;
    private String url;
    private CorsConfiguration corsConfig;

    @Override
    public String getName() {
        return Optional.ofNullable(name)
                .orElse(DEFAULT_NAME);
    }

    @Override
    public Boolean getEnabled() {
        return Optional.ofNullable(enabled)
                .orElse(DEFAULT_ENABLED);
    }

    public String getUrl() {
        return Optional.ofNullable(url)
                .orElse(DEFAULT_URL);
    }

    public CorsConfiguration getCorsConfig() {
        return Optional.ofNullable(corsConfig)
                .orElse(DEFAULT_CORS_CONFIG);
    }
}
