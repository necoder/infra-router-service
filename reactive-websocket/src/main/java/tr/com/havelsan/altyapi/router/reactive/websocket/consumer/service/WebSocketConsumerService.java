package tr.com.havelsan.altyapi.router.reactive.websocket.consumer.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;
import tr.com.havelsan.altyapi.router.api.model.RoutingMessage;
import tr.com.havelsan.altyapi.router.api.service.ConsumerService;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

@Log4j2
public class WebSocketConsumerService implements WebSocketHandler, ConsumerService {

    private final String name;
    private final Map<String, WebSocketSession> sessionMap;
    private Consumer<RoutingMessage<?>> messageConsumedEvent;

    public WebSocketConsumerService(String name) {
        this.name = name;
        this.sessionMap = new HashMap<>();
    }

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        this.sessionMap.putIfAbsent(session.getId(), session);
        return session
                .receive()
                .map(WebSocketMessage::getPayloadAsText)
                .doOnNext(message -> log.debug("Websocket session:{}, received message payload as text: {}.",
                        session.getId(),
                        message))
                .map(message -> new RoutingMessage<>(session.getId(), message))
                .doOnNext(messageConsumedEvent)
                .then();
    }

    @Override
    public void setMessageConsumedEvent(Consumer<RoutingMessage<?>> messageConsumedEvent) {
        this.messageConsumedEvent = messageConsumedEvent;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void dispose() {
        sessionMap
                .values()
                .forEach(this::closeSession);
    }

    private void closeSession(WebSocketSession session) {
        try {
            session.close();
        } catch (Exception e) {
            log.error("Unexpected error in WebsocketService during dispose for session:{}",
                    session.getId(),
                    e);
        }
    }
}