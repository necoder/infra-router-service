package tr.com.havelsan.altyapi.router.kafka.consumer.service;

import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import tr.com.havelsan.altyapi.router.api.model.RoutingMessage;
import tr.com.havelsan.altyapi.router.api.service.ConsumerService;

import java.time.Duration;
import java.util.function.Consumer;

@Log4j2
public class KafkaConsumerService implements ConsumerService {

    private static final long POLL_TIMEOUT = 100;

    private final String name;
    private final KafkaConsumer<String, Object> kafkaConsumer;

    private Boolean isListening;
    private Consumer<RoutingMessage<?>> messageConsumedEvent;

    public KafkaConsumerService(String name,
                                KafkaConsumer<String, Object> kafkaConsumer) {
        log.info("Kafka Consumer Service({}) is initializing..", name);
        this.name = name;
        this.isListening = true;
        this.kafkaConsumer = kafkaConsumer;
    }

    public void listen() {
        try {
            while (isListening) {
                ConsumerRecords<String, Object> records = kafkaConsumer.poll(Duration.ofMillis(POLL_TIMEOUT));
                records.forEach(record -> {
                    log.debug("Consumed Kafka Record key:{}, value:{}", record.key(), record.value());
                    messageConsumedEvent.accept(new RoutingMessage<>(record.key(), record.value()));
                });
            }
        } catch (Exception e) {
            log.error("Unexpected error in KafkaConsumerService({}) during listen.", name, e);
        } finally {
            log.info("Kafka Consumer({}) is closing..", name);
            try {
                kafkaConsumer.close();
            } catch (Exception e) {
                log.error("Unexpected error in KafkaConsumerService({}) during close.", name);
            }
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setMessageConsumedEvent(Consumer<RoutingMessage<?>> messageConsumedEvent) {
        this.messageConsumedEvent = messageConsumedEvent;
    }

    @Override
    public void dispose() {
        log.info("Kafka Consumer({}) is disposing..", name);
        isListening = false;
    }
}
