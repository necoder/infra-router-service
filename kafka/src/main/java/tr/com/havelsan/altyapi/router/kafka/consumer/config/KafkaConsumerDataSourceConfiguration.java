package tr.com.havelsan.altyapi.router.kafka.consumer.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import tr.com.havelsan.altyapi.router.api.config.DataSourceConfiguration;
import tr.com.havelsan.altyapi.router.kafka.model.KafkaDataSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@Configuration
@ConfigurationProperties(prefix = "router.consumer.kafka")
public class KafkaConsumerDataSourceConfiguration implements DataSourceConfiguration<KafkaDataSource> {
    private List<KafkaDataSource> dataSources;

    @Override
    public List<KafkaDataSource> getDataSources() {
        return Optional.ofNullable(dataSources)
                .orElse(new ArrayList<>());
    }
}
