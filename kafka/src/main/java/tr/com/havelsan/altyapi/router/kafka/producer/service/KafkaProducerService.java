package tr.com.havelsan.altyapi.router.kafka.producer.service;

import lombok.extern.log4j.Log4j2;
import org.apache.avro.Schema;
import org.springframework.kafka.core.KafkaTemplate;
import tr.com.havelsan.altyapi.router.api.model.RoutingMessage;
import tr.com.havelsan.altyapi.router.api.service.ProducerService;
import tr.com.havelsan.altyapi.router.kafka.model.SchemaType;
import tr.com.havelsan.altyapi.router.kafka.util.CustomRecordUtil;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

@Log4j2
public class KafkaProducerService implements ProducerService {

    private static final String KEY_FORMAT = "{0}-{1}"; // [topic-name]-[sender-token]

    private final String name;
    private final List<String> topicNameList;
    private final Schema schema;
    private final SchemaType schemaType;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public KafkaProducerService(String name,
                                List<String> topicNameList,
                                SchemaType schemaType,
                                String schemaPath,
                                KafkaTemplate<String, Object> kafkaTemplate) {
        log.info("Kafka Producer Service({}) is initializing..", name);
        this.name = name;
        this.topicNameList = topicNameList;
        this.schema = schemaType.isAvro() ? CustomRecordUtil.parseSchema(schemaPath) : null;
        this.schemaType = schemaType;
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void send(RoutingMessage<?> message) {
        topicNameList
                .stream()
                .peek(topicName -> log.debug("Kafka Producing Message(source:{}, payload:{}) sending to Kafka Topic({})..",
                        message.getSource(),
                        message.getPayload(),
                        topicName
                ))
                .forEach(topicName -> sendToTopic(topicName, message));
    }

    private void sendToTopic(String topicName, RoutingMessage<?> message) {
        try {
            kafkaTemplate.send(
                    topicName,
                    generateKey(
                            topicName,
                            message.getSource()
                    ),
                    prepareData(message.getPayload())
            );
        } catch (Exception e) {
            log.error("Error in KafkaProducerService({}) during send.", name);
        }
    }

    private String generateKey(String topicName, String senderToken) {
        return MessageFormat.format(KEY_FORMAT, topicName, senderToken);
    }

    @Override
    public String getName() {
        return name;
    }

    private Object prepareData(Object data) {
        return Optional.of(data)
                .filter(o -> schemaType.isAvro())
                .map(o -> CustomRecordUtil.createGenericRecord(schema, o))
                .orElse(data);
    }

    @Override
    public void dispose() {
        try {
            log.info("Kafka Producer Service({}) is disposing..", name);
            kafkaTemplate.destroy();
        } catch (Exception e) {
            log.error("Unexpected error in KafkaProducerService({}) during dispose.", name);
        }
    }
}
