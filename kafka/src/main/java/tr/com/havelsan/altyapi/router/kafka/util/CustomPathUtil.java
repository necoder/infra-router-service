package tr.com.havelsan.altyapi.router.kafka.util;

import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Optional;

@Log4j2
final class CustomPathUtil {
    public static Path toPath(String fileName) {
        log.debug("Converting fileName({}) to Path..", fileName);
        return Optional.ofNullable(fileName)
                .map(Paths::get)
                .filter(Files::exists)
                .orElseGet(() -> toPathForFullName(fileName));
    }

    private static Path toPathForFullName(String fullName) {
        return Optional.ofNullable(fullName)
                .map(ClassLoader.getSystemClassLoader()::getResource)
                .map(Objects::requireNonNull)
                .map(URL::getFile)
                .map(File::new)
                .map(File::toPath)
                .orElse(null);
    }

    public static String getFileAsString(Path path) {
        try {
            return Files.readString(path);
        } catch (IOException e) {
            log.error("Error reading file({}) as string", path, e);
            return null;
        }
    }
}
