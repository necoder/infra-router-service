package tr.com.havelsan.altyapi.router.kafka.model;

import java.util.Objects;

public enum SchemaType {
    JSON, AVRO, JSON_SCHEMA;

    public boolean isAvro() {
        return Objects.equals(this, AVRO);
    }
}
