package tr.com.havelsan.altyapi.router.kafka.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;

import java.util.LinkedHashMap;

@Log4j2
public final class ObjectMapperUtil {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static <T> T readObject(String line, Class<T> tClass) {
        try {
            return objectMapper.readValue(line, tClass);
        } catch (JsonProcessingException e) {
            log.error("Unexpected error in reading value with object mapper", e);
            return null;
        }
    }

    public static LinkedHashMap<?, ?> objectToMap(Object obj) {
        try {
            return readObject(objectMapper.writeValueAsString(obj), LinkedHashMap.class);
        } catch (JsonProcessingException e) {
            log.error("Unexpected error in converting from object to map with object mapper", e);
            return null;
        }
    }
}
