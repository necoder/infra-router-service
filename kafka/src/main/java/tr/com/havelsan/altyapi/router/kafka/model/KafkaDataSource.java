package tr.com.havelsan.altyapi.router.kafka.model;

import lombok.Data;
import tr.com.havelsan.altyapi.router.api.config.DataSource;

import java.util.List;
import java.util.Optional;

@Data
public class KafkaDataSource implements DataSource {
    private final static String DEFAULT_TOPIC_NAME = "default-topic";
    private final static String DEFAULT_SCHEMA_REGISTRY_URL = "http://localhost:8081";
    private final static SchemaType DEFAULT_SCHEMA_TYPE = SchemaType.AVRO;
    private final static String DEFAULT_SCHEMA_FILE_NAME = null;
    private final static String DEFAULT_NAME = "kafka-service";
    private final static Boolean DEFAULT_ENABLED = false;

    private String name;
    private Boolean enabled;
    private List<String> topicNameList;
    private SchemaType schemaType;
    private String schemaFileName;
    private String schemaRegistryUrl;

    public List<String> getTopicNameList() {
        return Optional.ofNullable(topicNameList)
                .orElse(List.of(DEFAULT_TOPIC_NAME));
    }

    public SchemaType getSchemaType() {
        return Optional.ofNullable(schemaType)
                .orElse(DEFAULT_SCHEMA_TYPE);
    }

    public String getSchemaFileName() {
        return Optional.ofNullable(schemaFileName)
                .orElse(DEFAULT_SCHEMA_FILE_NAME);
    }

    public String getSchemaRegistryUrl() {
        return Optional.ofNullable(schemaRegistryUrl)
                .orElse(DEFAULT_SCHEMA_REGISTRY_URL);
    }

    @Override
    public String getName() {
        return Optional.ofNullable(name)
                .orElse(DEFAULT_NAME);
    }

    @Override
    public Boolean getEnabled() {
        return Optional.ofNullable(enabled)
                .orElse(DEFAULT_ENABLED);
    }
}
