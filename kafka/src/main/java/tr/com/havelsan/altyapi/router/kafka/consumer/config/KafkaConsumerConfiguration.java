package tr.com.havelsan.altyapi.router.kafka.consumer.config;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import tr.com.havelsan.altyapi.router.api.IRouterManager;
import tr.com.havelsan.altyapi.router.api.config.DataSource;
import tr.com.havelsan.altyapi.router.api.config.ServiceConfiguration;
import tr.com.havelsan.altyapi.router.kafka.consumer.service.KafkaConsumerService;
import tr.com.havelsan.altyapi.router.kafka.model.SchemaType;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;

@Configuration
@Log4j2
public class KafkaConsumerConfiguration implements ServiceConfiguration {
    private final KafkaConsumerDataSourceConfiguration dataSourceConfig;

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    @Value("${spring.kafka.consumer.group-id}")
    private String groupId;

    public KafkaConsumerConfiguration(KafkaConsumerDataSourceConfiguration dataSourceConfig) {
        this.dataSourceConfig = dataSourceConfig;
    }

    @Autowired
    @Override
    public void registerServicesToRouter(IRouterManager routerManager) {
        dataSourceConfig.getDataSources()
                .stream()
                .peek(dataSource -> log.info("Data Source name:{}, enabled:{}.", dataSource.getName(), dataSource.getEnabled()))
                .filter(DataSource::getEnabled)
                .map(dataSource -> new KafkaConsumerService(
                        dataSource.getName(),
                        createKafkaConsumer(
                                dataSource.getTopicNameList(),
                                dataSource.getSchemaType()
                        )
                ))
                .peek(service -> log.info("Kafka Consumer Service({}) is registering to router..", service.getName()))
                .peek(routerManager::registerConsumer)
                .forEach(consumerService -> CompletableFuture.runAsync(consumerService::listen));
    }


    public KafkaConsumer<String, Object> createKafkaConsumer(List<String> topicNameList,
                                                             SchemaType schemaType) {
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, getValueDeserializerClass(schemaType));
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);

        KafkaConsumer<String, Object> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(topicNameList);
        return consumer;
    }

    private Class<?> getValueDeserializerClass(SchemaType schemaType) {
        return schemaType.isAvro()
                ? KafkaAvroDeserializer.class
                : JsonDeserializer.class;
    }
}
