package tr.com.havelsan.altyapi.router.kafka.producer.config;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import tr.com.havelsan.altyapi.router.api.IRouterManager;
import tr.com.havelsan.altyapi.router.api.config.DataSource;
import tr.com.havelsan.altyapi.router.api.config.ServiceConfiguration;
import tr.com.havelsan.altyapi.router.kafka.model.SchemaType;
import tr.com.havelsan.altyapi.router.kafka.producer.service.KafkaProducerService;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@Configuration
public class KafkaProducerConfiguration implements ServiceConfiguration {
    private final KafkaProducerDataSourceConfiguration dataSourceConfig;

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    public KafkaProducerConfiguration(KafkaProducerDataSourceConfiguration dataSourceConfig) {
        this.dataSourceConfig = dataSourceConfig;
    }

    @Autowired
    @Override
    public void registerServicesToRouter(IRouterManager routerManager) {
        dataSourceConfig.getDataSources()
                .stream()
                .peek(dataSource -> log.info("Data Source name:{}, enabled:{}.",
                        dataSource.getName(),
                        dataSource.getEnabled()))
                .filter(DataSource::getEnabled)
                .map(dataSource -> new KafkaProducerService(
                        dataSource.getName(),
                        dataSource.getTopicNameList(),
                        dataSource.getSchemaType(),
                        dataSource.getSchemaFileName(),
                        createKafkaTemplate(
                                dataSource.getSchemaType(),
                                dataSource.getSchemaRegistryUrl()
                        )
                ))
                .peek(service -> log.info("Kafka Producer Service({}) is registering to router..", service.getName()))
                .forEach(routerManager::registerProducer);
    }

    public KafkaTemplate<String, Object> createKafkaTemplate(SchemaType schemaType,
                                                             String schemaRegistryUrl) {
        return new KafkaTemplate<>(createProducerFactory(schemaType, schemaRegistryUrl));
    }

    public ProducerFactory<String, Object> createProducerFactory(SchemaType schemaType,
                                                                 String schemaRegistryUrl) {
        Map<String, Object> configMap = new HashMap<>();
        configMap.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        configMap.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configMap.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, getValueSerializerClass(schemaType));
        configMap.put("schema.registry.url", schemaRegistryUrl);
        return new DefaultKafkaProducerFactory<>(configMap);
    }

    private Class<?> getValueSerializerClass(SchemaType schemaType) {
        return schemaType.isAvro()
                ? KafkaAvroSerializer.class
                : JsonSerializer.class;
    }
}
