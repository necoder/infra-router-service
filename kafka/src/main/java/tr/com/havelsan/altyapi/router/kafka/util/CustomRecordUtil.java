package tr.com.havelsan.altyapi.router.kafka.util;

import lombok.extern.log4j.Log4j2;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.util.Map;
import java.util.Optional;

@Log4j2
public final class CustomRecordUtil {
    public static Schema createSchema(String json) {
        return new Schema.Parser().parse(json);
    }

    public static Schema parseSchema(String schemaPath) {
        log.debug("Parsing schema (path:{}).", schemaPath);
        return Optional.ofNullable(schemaPath)
                .map(CustomPathUtil::toPath)
                .map(CustomPathUtil::getFileAsString)
                .map(CustomRecordUtil::createSchema)
                .orElse(Schema.create(Schema.Type.NULL));
    }

    public static Object createGenericRecord(Schema schema, Object data) {
        log.debug("Creating Generic Record with schema(name:{}, type:{}).", schema.getName(), schema.getType());
        return Optional.ofNullable(data)
                .map(ObjectMapperUtil::objectToMap)
                .map(map -> createGenericRecord(schema, map))
                .orElse(null);
    }

    private static GenericRecord createGenericRecord(Schema schema, Map<?, ?> fieldMap) {
        return schema.getFields()
                .stream()
                .reduce(new GenericData.Record(schema),
                        (record, field) -> putRecord(
                                record,
                                field.name(),
                                field.schema(),
                                fieldMap.get(field.name())
                        ),
                        (oldRecord, newRecord) -> oldRecord = newRecord
                );
    }

    private static GenericData.Record putRecord(GenericData.Record record,
                                                String name,
                                                Schema fieldSchema,
                                                Object data) {
        switch (fieldSchema.getType()) {
            case RECORD:
                data = createGenericRecord(fieldSchema, (Map<?, ?>) data);
                break;
            case ENUM:
                data = new GenericData.EnumSymbol(fieldSchema, data);
                break;
        }
        record.put(name, data);
        return record;
    }
}
