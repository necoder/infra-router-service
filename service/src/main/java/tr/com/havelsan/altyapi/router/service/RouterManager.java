package tr.com.havelsan.altyapi.router.service;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import tr.com.havelsan.altyapi.router.api.IRouterManager;
import tr.com.havelsan.altyapi.router.api.service.ConsumerService;
import tr.com.havelsan.altyapi.router.api.service.ProducerService;
import tr.com.havelsan.altyapi.router.service.config.RouterConfiguration;
import tr.com.havelsan.altyapi.router.service.config.RouterMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class RouterManager implements IRouterManager {
    private final List<ConsumerService> registeredConsumers;
    private final List<ProducerService> registeredProducers;

    public RouterManager() {
        this.registeredConsumers = new ArrayList<>();
        this.registeredProducers = new ArrayList<>();
    }

    @Override
    public void registerProducer(ProducerService producerService) {
        registeredProducers.add(producerService);
    }

    @Override
    public void registerConsumer(ConsumerService consumerService) {
        registeredConsumers.add(consumerService);
    }

    @Bean
    public List<RouterService> routerManagerList(RouterConfiguration routerMapping) {
        return routerMapping.getMappings()
                .stream()
                .map(this::createRouterService)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private Optional<RouterService> createRouterService(RouterMapping mapping) {
        return Optional.of(mapping.getConsumer())
                .map(this::findConsumerService)
                .filter(Optional::isPresent)
                .map(consumerService -> new RouterService(
                        consumerService.get(),
                        findProducerServices(mapping.getProducerList())
                ));
    }

    private Optional<ConsumerService> findConsumerService(String consumerName) {
        return registeredConsumers
                .stream()
                .filter(consumerService -> Objects.equals(consumerName, consumerService.getName()))
                .findFirst();
    }

    private List<ProducerService> findProducerServices(List<String> producerNameList) {
        return registeredProducers
                .stream()
                .filter(producerService -> producerNameList.contains(producerService.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public void dispose() {
        registeredConsumers.forEach(ConsumerService::dispose);
        registeredProducers.forEach(ProducerService::dispose);
    }
}
