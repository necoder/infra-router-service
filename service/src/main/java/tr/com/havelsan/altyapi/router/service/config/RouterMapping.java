package tr.com.havelsan.altyapi.router.service.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RouterMapping {
    private final static String DEFAULT_CONSUMER = "no-consumer";

    private String consumer;
    private List<String> producerList;

    public RouterMapping() {
    }

    public String getConsumer() {
        return Optional.ofNullable(consumer)
                .orElse(DEFAULT_CONSUMER);
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

    public List<String> getProducerList() {
        return Optional.ofNullable(producerList)
                .orElse(new ArrayList<>());
    }

    public void setProducerList(List<String> producerList) {
        this.producerList = producerList;
    }
}
