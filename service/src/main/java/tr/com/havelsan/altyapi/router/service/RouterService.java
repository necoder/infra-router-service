package tr.com.havelsan.altyapi.router.service;

import tr.com.havelsan.altyapi.router.api.model.RoutingMessage;
import tr.com.havelsan.altyapi.router.api.service.ConsumerService;
import tr.com.havelsan.altyapi.router.api.service.IRouterService;
import tr.com.havelsan.altyapi.router.api.service.ProducerService;

import java.util.List;

public class RouterService implements IRouterService {
    private final List<ProducerService> producerServiceList;

    public RouterService(ConsumerService consumerService, List<ProducerService> producerServiceList) {
        this.producerServiceList = producerServiceList;
        consumerService.setMessageConsumedEvent(this::route);
    }

    public void route(RoutingMessage<?> message) {
        producerServiceList.forEach(producerService -> producerService.send(message));
    }
}
