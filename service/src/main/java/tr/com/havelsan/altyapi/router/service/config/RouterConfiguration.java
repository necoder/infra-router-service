package tr.com.havelsan.altyapi.router.service.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Configuration
@ConfigurationProperties(prefix = "router")
public class RouterConfiguration {
    private List<RouterMapping> mappings;

    public RouterConfiguration() {
    }

    public List<RouterMapping> getMappings() {
        return Optional.ofNullable(mappings)
                .orElse(new ArrayList<>());
    }

    public void setMappings(List<RouterMapping> mappings) {
        this.mappings = mappings;
    }
}
