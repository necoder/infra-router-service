package tr.com.havelsan.altyapi.router.api.config;

public interface DataSource {
    String getName();

    Boolean getEnabled();
}
