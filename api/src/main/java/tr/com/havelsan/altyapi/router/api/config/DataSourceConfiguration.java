package tr.com.havelsan.altyapi.router.api.config;

import java.util.List;


public interface DataSourceConfiguration<T extends DataSource> {
    List<T> getDataSources();
}
