package tr.com.havelsan.altyapi.router.api.model;

public class RoutingMessage<T> {
    private final String source;
    private final T payload;

    public RoutingMessage(String source, T payload) {
        this.source = source;
        this.payload = payload;
    }

    public T getPayload() {
        return payload;
    }

    public String getSource() {
        return source;
    }
}
