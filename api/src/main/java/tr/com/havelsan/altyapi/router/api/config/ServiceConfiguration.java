package tr.com.havelsan.altyapi.router.api.config;

import tr.com.havelsan.altyapi.router.api.IRouterManager;

public interface ServiceConfiguration {
    void registerServicesToRouter(IRouterManager routerManager);
}
