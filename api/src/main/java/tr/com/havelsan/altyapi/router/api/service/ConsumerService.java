package tr.com.havelsan.altyapi.router.api.service;

import tr.com.havelsan.altyapi.router.api.model.RoutingMessage;

import java.util.function.Consumer;

public interface ConsumerService extends BaseService {
    void setMessageConsumedEvent(Consumer<RoutingMessage<?>> messageConsumedEvent);
}
