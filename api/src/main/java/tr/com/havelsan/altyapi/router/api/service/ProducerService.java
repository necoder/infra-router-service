package tr.com.havelsan.altyapi.router.api.service;

import tr.com.havelsan.altyapi.router.api.model.RoutingMessage;

public interface ProducerService extends BaseService {
    void send(RoutingMessage<?> message);
}
