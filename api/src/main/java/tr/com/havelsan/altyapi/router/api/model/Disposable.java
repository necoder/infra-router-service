package tr.com.havelsan.altyapi.router.api.model;

public interface Disposable {
    void dispose();
}
