package tr.com.havelsan.altyapi.router.api;

import tr.com.havelsan.altyapi.router.api.model.Disposable;
import tr.com.havelsan.altyapi.router.api.service.ConsumerService;
import tr.com.havelsan.altyapi.router.api.service.ProducerService;

public interface IRouterManager extends Disposable {
    void registerProducer(ProducerService producerService);

    void registerConsumer(ConsumerService consumerService);
}
