package tr.com.havelsan.altyapi.router.api.service;

import tr.com.havelsan.altyapi.router.api.model.RoutingMessage;

public interface IRouterService {
    void route(RoutingMessage<?> message);
}
