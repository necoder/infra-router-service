package tr.com.havelsan.altyapi.router.api.service;

import tr.com.havelsan.altyapi.router.api.model.Disposable;

public interface BaseService extends Disposable {
    String getName();
}
